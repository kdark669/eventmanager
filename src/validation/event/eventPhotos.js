export default function eventPhotos(values) {
    let errors = {};

    if (!values.eimage) {
        errors.eimage = 'Images is required'
    }
    return errors;
};