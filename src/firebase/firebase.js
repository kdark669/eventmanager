//your firebase credentials

import * as f from 'firebase'

const firebaseConfig = {
    apiKey: "AIzaSyDiKhmIYWuwimE3YToG-mItZYNjJ3K3JBg",
    authDomain: "eventmanage-d3adb.firebaseapp.com",
    databaseURL: "https://eventmanage-d3adb.firebaseio.com",
    projectId: "eventmanage-d3adb",
    storageBucket: "eventmanage-d3adb.appspot.com",
    messagingSenderId: "291243980538",
    appId: "1:291243980538:web:97b16bc329d71a2c6efb43",
    measurementId: "G-NEPPSSYZ7M"
};

const firebase = f.initializeApp(firebaseConfig);
const database = f.database();
firebase.analytics();

export { firebase, database };