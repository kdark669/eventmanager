import React from 'react'
import PropTypes from 'prop-types'
import BreadCumb from '../../../../component/BreadCumb'
import EventPhotoForm from './containers/Associate/photos/EventPhotoForm'


const EventGallery = props => {
    const { event_id } = props
    return (
        <>
            <BreadCumb >
                <div className="upload-images">
                    <EventPhotoForm event_id={event_id}/>
                </div>
            </BreadCumb>
        </>
    )
}

EventGallery.propTypes = {
    event_id:PropTypes.string.isRequired
}

export default EventGallery
