import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import useForm from '../../../../../../../customHooks/useForm'
import eventPhotos from '../../../../../../../validation/event/eventPhotos'
import { connect } from 'react-redux'
import * as actions from '../../../../../../../redux/action'


const EventPhotoForm = props => {
    const { event_id, uploadEventPhotos, fetchEventPhotos } = props
    const [eimage, setImages] = useState([])

    const uploadImages = (e) => {
        e.preventDefault();
        uploadEventPhotos(event_id, eimage)
    }

    const handleChange = (e) => {
        e.persist();
        setImages(e.target.files[0]);
    }
    useEffect(() => {
        fetchEventPhotos(event_id)
    },[event_id])

    return (
        <form className="form-group" onSubmit={uploadImages}>
            <input
                type="file"
                className="input-images"
                multiple name="eimage"
                onChange={handleChange} />

            <button className="btn btn-sm btn-success">
                Upload
            </button>
        </form>
    )
}

EventPhotoForm.propTypes = {
    event_id:PropTypes.string.isRequired
}
const mapDispatchToProps = dispatch => {
    return {
        uploadEventPhotos: (e_id, eimage) => dispatch(actions.uploadEventPhotos(e_id, eimage)),
    }
}
export default connect(null, mapDispatchToProps)(EventPhotoForm)
